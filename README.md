# docker-k8s-ws
A small docker-k8s-workshop

# Pre-requisites

- Linux Shell (Preferebly any linux distributions, windows users can use WSL2. Please check https://docs.microsoft.com/en-us/windows/wsl/install-win10)
- Docker-desktop (https://www.docker.com/products/docker-desktop )
- Azure CLI (https://docs.microsoft.com/en-us/cli/azure/install-azure-cli )
- Azure Subscription for deploying app in Azure
- Git (https://git-scm.com/)
- Visual Studio code with ***[Microsoft Kubernetes extension](https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools)*** installed
